let showPass = document.querySelectorAll("#eye");
showPass.forEach(item =>
    item.addEventListener('click', changeType)
);
// Показати, сховати значення Пароля
function changeType() {
    let input = this.closest('.input-wrapper').querySelector('.password');
    let i = this.closest('.input-wrapper').querySelector('#eye');
    if (input.type === 'password') {
        input.type = 'text';
        i.classList.remove('fa-eye');
        i.classList.add('fa-eye-slash');
    } else {
        input.type = 'password';
        i.classList.remove('fa-eye-slash');
        i.classList.add('fa-eye');
    }
}

let form = document.querySelector('.password-form');
let password1 = document.querySelector('#password1');
let password2 = document.querySelector('#password2');

// Кнопка Підтвердити
form.addEventListener('submit', (e) => {
    e.preventDefault();
    checkInputs();
});
// Перевірка значень Пароля1 і Пароля2
function checkInputs () {
    let password1Value = password1.value.trim();
    let password2Value = password2.value.trim();
    if (password1Value === '' || password2Value === '') {
        alert('Can not be blank. Try again');
    } else if (password1Value !== password2Value) {
        let error = document.querySelector('#error');
        error.innerHTML = 'Потрібно ввести однакові значення';
        password2.after(error);
    } else {
        alert('You are welcome');
    }
    password1.addEventListener('focus', () => error.innerHTML = "");
    password2.addEventListener('focus', () => error.innerHTML = "");
}


// HW14
let themeSwitchers = document.querySelectorAll('.btn-theme');

themeSwitchers.forEach(switcher => {
    switcher.addEventListener('click', function () {
        applyTheme(this.dataset.theme);
        localStorage.setItem('theme', this.dataset.theme);
    })
});

function applyTheme(themeName) {
    let themeUrl = `./theme-${themeName}.css`;
    document.querySelector('[title="theme"]').setAttribute('href', themeUrl);
};

let activeTheme = localStorage.getItem('theme');
if (activeTheme === null) {
    applyTheme('light');
} else {
    applyTheme(activeTheme);
}